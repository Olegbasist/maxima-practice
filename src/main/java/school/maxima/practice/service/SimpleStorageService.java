package school.maxima.practice.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface SimpleStorageService {

    void init ();
    String saveFile (MultipartFile file);
    Resource downloadFile (String fileName);
}