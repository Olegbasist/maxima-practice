package school.maxima.practice.service;

import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import school.maxima.practice.StorageProperties;
import school.maxima.practice.exeption.FileNotFoundException;
import school.maxima.practice.exeption.StorageException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

@Service
public class FileStorageService implements SimpleStorageService {

    private final Path location;

    @Autowired
    public FileStorageService(StorageProperties storageProperties) {
        this.location = Paths.get(storageProperties.getLocation()).toAbsolutePath().normalize();
    }

    @Override
    @PostConstruct
    public void init() {
        try {
            Files.createDirectories(location);
        }
        catch (IOException e) {
            throw new StorageException("Could not initialize storage", e);
        }
    }

    @Override
    public String saveFile(MultipartFile file) {
        try {
            String uuid = UUID.randomUUID().toString();
            String fileName = uuid + "." + file.getOriginalFilename();
            Path difFile = this.location.resolve(fileName);
            Files.copy(file.getInputStream(), difFile, StandardCopyOption.REPLACE_EXISTING);
            return fileName;

        } catch (Exception e) {
            throw new StorageException("Could not upload file");
        }
    }

    @Override
    public Resource downloadFile(String fileName) {
        try {
            Path file = this.location.resolve(fileName).normalize();
            Resource resource = new UrlResource(file.toUri());

            if (resource.exists() || resource.isReadable()) {
                return resource;
            }
            else {
                throw new FileNotFoundException("Could not find file");
            }
        }
        catch (MalformedURLException e) {
            throw new FileNotFoundException("Could not download file");
        }

    }
}