
package school.maxima.practice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import school.maxima.practice.service.FileResponse;
import school.maxima.practice.service.FileStorageService;


@Controller
@RequestMapping("/upload")
public class FileUploadController {

    @Autowired
    private FileStorageService fileStorageService;

    @GetMapping()
    public String rootMassage (){
        return ("Available requests: "
                +"\n"
        );
    }

    @PostMapping("/file")
    public ResponseEntity<FileResponse> uploadSingleFile (@RequestParam("file") MultipartFile file) {
        String uploadedFileName = fileStorageService.saveFile(file);
        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/upload/file/")
                .path(uploadedFileName)
                .toUriString();

        FileResponse fileResponse = new FileResponse(uploadedFileName,fileDownloadUri, "File uploaded successfully");
        return ResponseEntity.status(HttpStatus.OK).body(fileResponse);
    }

    @GetMapping("/file/{filename:.+}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String filename) {

        Resource resource = fileStorageService.downloadFile(filename);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

}