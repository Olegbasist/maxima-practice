package school.maxima.practice.exeption;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;
import java.util.List;


public class ResponseError {
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    private LocalDateTime timestamp;
    private String message;
    private List erros;

    public ResponseError(LocalDateTime timestamp, String message, List erros) {
        this.timestamp = timestamp;
        this.message = message;
        this.erros = erros;
    }
}
